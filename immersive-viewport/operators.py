import bpy
import OpenGL
import numpy

from bpy.types import Operator
from bpy.props import BoolProperty
from bgl import *
from OpenGL.GL import *
from OpenGL.GL import shaders
from math import pi, atan
from mathutils import Vector

from . import glslShaders
from .glView import GLView


class ImmersiveView3DToggle(Operator):
    bl_idname = "immersive.toggle"
    bl_label = "View3D immersive draw toggle"

    no_window = BoolProperty(name="no_window", default=False)

    def execute(self, context):
        for wm in bpy.data.window_managers:
            for window in wm.windows:
                if window.screen.immersive_viewport.enabled and window.screen.immersive_viewport.active:
                    bpy.ops.immersive.enable({
                        'window_manager': context.window_manager,
                        'window': window,
                        'screen': window.screen,
                        'area': window.screen.areas[0],
                        'region': context.region,
                        'scene': context.scene,
                        'blend_data': context.blend_data
                    }, 'INVOKE_DEFAULT')
                    return {'FINISHED'}

        if self.no_window:
            return {'FINISHED'}

        # Duplicate it in a new window
        bpy.ops.screen.area_dupli({
            'window': context.window,
            'screen': context.screen,
            'area': context.area,
            'region': context.region,
            'scene': context.scene,
            'blend_data': context.blend_data
        }, 'INVOKE_DEFAULT')

        # Reshow everything from the current window assuming its now last in the list
        windows = bpy.data.window_managers[0].windows
        window = windows[len(windows) - 1]

        # This will change the context's screen, so the window's screen and area will be different after this call
        bpy.ops.screen.screen_full_area({
            'window': window,
            'screen': window.screen,
            'area': window.screen.areas[0],
            'region': None,  # area.regions[4],
            'scene': context.scene,
            'blend_data': context.blend_data
        }, use_hide_panels=True)

        bpy.ops.immersive.enable({
            'window_manager': context.window_manager,
            'window': window,
            'screen': window.screen,
            'area': window.screen.areas[0],
            'region': None,  # area.regions[4],
            'scene': context.scene,
            'blend_data': context.blend_data
        }, 'INVOKE_DEFAULT')

        for wm in bpy.data.window_managers:
            for window in wm.windows:
                window.screen.immersive_viewport.active = False
        window.screen.immersive_viewport.enabled = True
        window.screen.immersive_viewport.active = True

        return {'FINISHED'}


class ImmersiveView3D(Operator):
    bl_idname = "immersive.enable"
    bl_label = "View3D immersive draw"

    _anchor = ""
    _renderMode = 0
    _renderWidth = 1024
    _renderHeight = 1024
    _fov = 210.0

    _shader = None
    _currentArea = None
    _glViews = []

    _near = 0.1
    _far = 100.0

    _shiftFromCenter = Vector((0.0, 0.0, 0.0))

    @staticmethod
    def shiftCameraPosition(context, x, y, z):
        '''
        Shift the camera center by the given values, without moving
        the Blender Camera object: only the rendering is impacted
        '''
        scene = context.scene
        camera = bpy.types.Camera(scene.camera)
        cameraPosition = camera.matrix_world.translation

        glViews = ImmersiveView3D._glViews
        for glView in glViews:
                glView.shiftPosition(x, y, z)

    @staticmethod
    def drawCallback(self, context):
        pos = ImmersiveView3D._shiftFromCenter
        ImmersiveView3D.shiftCameraPosition(context, pos.x, pos.y, pos.z)

        for glView in ImmersiveView3D._glViews:
            glView.draw(context)
        self.openglDraw(self, context, self._glViews)

    @staticmethod
    def handleAdd(self, context):
        ImmersiveView3D._handle_draw = bpy.types.SpaceView3D.draw_handler_add(
            self.drawCallback, (self, context), 'WINDOW', 'POST_PIXEL')

    @staticmethod
    def handleRemove(self):
        if ImmersiveView3D._handle_draw is not None:
            bpy.types.SpaceView3D.draw_handler_remove(ImmersiveView3D._handle_draw, 'WINDOW')
            ImmersiveView3D._handle_draw = None

    @staticmethod
    def updateParameters(context):
        if bpy.context.scene.world is None:
            return

        imm = bpy.context.user_preferences.addons[__package__].preferences
        ImmersiveView3D._renderWidth = imm.subrenderResolution
        ImmersiveView3D._renderHeight = imm.subrenderResolution
        ImmersiveView3D._fov = imm.fieldOfView
        ImmersiveView3D._anchor = imm.anchorObject
        # The following test could certainly be replaced 
        if imm.projectionType == "Spherical":
            ImmersiveView3D._renderMode = 0
        elif imm.projectionType == "Equirectangular":
            ImmersiveView3D._renderMode = 1
        elif imm.projectionType == "Cubemap":
            ImmersiveView3D._renderMode = 2

        # Center shifting only makes sense in spherical mode
        if ImmersiveView3D._renderMode == 0:
            ImmersiveView3D._shiftFromCenter.x = imm.shiftFromCenter[0] / imm.domeRadius
            ImmersiveView3D._shiftFromCenter.y = imm.shiftFromCenter[1] / imm.domeRadius
            ImmersiveView3D._shiftFromCenter.z = imm.shiftFromCenter[2] / imm.domeRadius
        else:
            ImmersiveView3D._shiftFromCenter.x = 0.0
            ImmersiveView3D._shiftFromCenter.y = 0.0
            ImmersiveView3D._shiftFromCenter.z = 0.0

    @staticmethod
    def openglDraw(self, context, glViews):
        if not context.area or self._currentArea != context.area.as_pointer():
            return

        if self._shader is None:
            self.initGL()
            return

        if bpy.context.scene.world is None:
            return

        imm = bpy.context.user_preferences.addons[__package__].preferences

        # Get the current parameters
        self.updateParameters(context)

        glDisable(GL_DEPTH_TEST)
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT)

        viewport = glGetIntegerv(GL_VIEWPORT)

        # For the spherical view, we want the viewport to be square
        if imm.projectionType == "Spherical":
            minDim = min(viewport[2], viewport[3])
            if minDim == viewport[2]:
                shift = int((viewport[3] - viewport[2]) / 2)
                viewportDims = [viewport[0], viewport[1] + shift, viewport[2], viewport[2]]
            else:
                shift = int((viewport[2] - viewport[3]) / 2)
                viewportDims = [viewport[0] + shift, viewport[1], viewport[3], viewport[3]]
        else:
            viewportDims = [viewport[0], viewport[1], viewport[2], viewport[3]]
        
        glViewport(viewportDims[0], viewportDims[1], viewportDims[2], viewportDims[3])
        glScissor(viewportDims[0], viewportDims[1], viewportDims[2], viewportDims[3])

        activeProgram = glGetIntegerv(GL_CURRENT_PROGRAM)
        glUseProgram(self._shader)

        activeTextures = []
        for i in range(0, 6):
            glActiveTexture(GL_TEXTURE0 + i)
            # Get the active texture for further resetting
            activeTextures.append(glGetIntegerv(GL_TEXTURE_BINDING_2D))
            glBindTexture(GL_TEXTURE_2D, glViews[i].getTexture())
            glUniform1i(self._texUniforms[i], i)

        glUniform1f(self._fovUniform, self._fov)
        glUniform1i(self._projectionUniform, self._renderMode)
        glUniform3f(self._positionUniform,
                    ImmersiveView3D._shiftFromCenter.x,
                    ImmersiveView3D._shiftFromCenter.y,
                    ImmersiveView3D._shiftFromCenter.z)

        glBindVertexArray(self._vao)
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4)
        glBindVertexArray(0)

        # Reset the state
        for i in range(0, 6):
            glActiveTexture(GL_TEXTURE0 + i)
            glBindTexture(GL_TEXTURE_2D, activeTextures[i])
        glActiveTexture(GL_TEXTURE0)
        glUseProgram(activeProgram)

        glViewport(viewport[0], viewport[1], viewport[2], viewport[3])
        glScissor(viewport[0], viewport[1], viewport[2], viewport[3])

    @staticmethod
    def compileShader(source, shaderType):
        """ This was copied from PyOpenGL source, to work around a bug..."""
        if isinstance(source, str):
            source = [source]
        shader = glCreateShader(shaderType)
        glShaderSource(shader, source)

        try:
            glCompileShader(shader)
            result = glGetShaderiv(shader, GL_COMPILE_STATUS)
        except:
            result = None

        if not(result):
            raise RuntimeError(
                """Shader compile failure (%s): %s"""%(
                    result,
                    glGetShaderInfoLog(shader),
                ),
                source,
                shaderType,
            )
        return shader

    def initGL(self):
        vertexShader = ImmersiveView3D.compileShader(glslShaders.vsQuad, GL_VERTEX_SHADER)
        fragmentShader = ImmersiveView3D.compileShader(glslShaders.fsImmersive, GL_FRAGMENT_SHADER)

        self._shader = shaders.compileProgram(vertexShader, fragmentShader)
        self._fovUniform = glGetUniformLocation(self._shader, b"_fov")
        self._positionUniform = glGetUniformLocation(self._shader, b"_position")
        self._projectionUniform = glGetUniformLocation(self._shader, b"_renderMode")
        self._texUniforms = []
        for i in range(0, 6):
            self._texUniforms.append(glGetUniformLocation(self._shader, bytes("_texture" + str(i), 'utf-8')))

        self._vao = glGenVertexArrays(1)
        glBindVertexArray(self._vao)
        glBindVertexArray(0)

    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D'

    def modal(self, context, event):
        if context.area and self._currentArea == context.area.as_pointer():
            context.area.tag_redraw()

        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        if ImmersiveView3D._currentArea is not None:  # == context.area.as_pointer():
            self.cancel(context)
            return {'FINISHED'}

        elif ImmersiveView3D._currentArea is None:
            from math import radians
            from mathutils import Matrix

            self.updateParameters(context)

            # Set render shading to Material
            spaces_3d_view = [space for space in context.area.spaces if space.type == 'VIEW_3D']
            for space in spaces_3d_view:
                space.viewport_shade = 'MATERIAL'
                space.fx_settings.use_ssao = True

            # Add horizontal views
            for i in range(0, 4):
                rotationMatrix = Matrix.Rotation(radians(90.0 * i), 4, 'Y')
                ImmersiveView3D._glViews.append(GLView(
                                                    context,
                                                    ImmersiveView3D._renderWidth,
                                                    ImmersiveView3D._renderWidth,
                                                    ImmersiveView3D._anchor,
                                                    rotationMatrix
                                                    ))
            # Add top and bottom
            ImmersiveView3D._glViews.append(GLView(
                                                context,
                                                ImmersiveView3D._renderWidth,
                                                ImmersiveView3D._renderWidth,
                                                ImmersiveView3D._anchor,
                                                Matrix.Rotation(radians(90.0), 4, 'X')
                                                ))
            ImmersiveView3D._glViews.append(GLView(
                                                context,
                                                ImmersiveView3D._renderWidth,
                                                ImmersiveView3D._renderWidth,
                                                ImmersiveView3D._anchor,
                                                Matrix.Rotation(radians(-90.0), 4, 'X')
                                                ))

            if self._glViews[0]:
                self.report({'INFO'}, "Offscreen buffers successfully initialized")
            else:
                self.report({'ERROR'}, "Error initializing offscreen buffer. More details in the console.")
                return {'CANCELLED'}

            ImmersiveView3D._currentArea = context.area.as_pointer()
            ImmersiveView3D.handleAdd(self, context)
            context.window_manager.modal_handler_add(self)

            return {'RUNNING_MODAL'}
        else:
            return {'FINISHED'}
                
    def cancel(self, context):
        ImmersiveView3D.handleRemove(self)
        ImmersiveView3D._glViews.clear()
        ImmersiveView3D._currentArea = None

        if context.area:
            context.area.tag_redraw()
