vsQuad = '''
    #version 330 compatibility
    
    const vec2 vertices[4] = vec2[]
    (
        vec2(-1.0, -1.0),
        vec2(1.0, -1.0),
        vec2(-1.0, 1.0),
        vec2(1.0, 1.0)
    );
    
    const vec2 uvs[4] = vec2[]
    (
        vec2(0.0, 0.0),
        vec2(1.0, 0.0),
        vec2(0.0, 1.0),
        vec2(1.0, 1.0)
    );
    
    out vec2 _uv;
    
    void main()
    {
        gl_Position = vec4(vertices[gl_VertexID], 0.0, 1.0);
        _uv = uvs[gl_VertexID];
    }
'''


fsImmersive = '''
    #version 330 compatibility

    #define PI 3.141592653589793
    #define HALFPI 1.5707963268 
    #define SQRT2 1.4142135623730951
    
    uniform sampler2D _texture0;
    uniform sampler2D _texture1;
    uniform sampler2D _texture2;
    uniform sampler2D _texture3;
    uniform sampler2D _texture4;
    uniform sampler2D _texture5;
    
    uniform int _renderMode = 0; // 0 for dome, 1 for pano, 2 for cubemap
    uniform float _fov = 180.f;
    uniform vec3 _position = vec3(0.0, 0.0, 0.0); // Normalized position inside the dome, default to center
    
    in vec2 _uv;
    out vec4 _color;

    vec4 renderDome()
    {
        vec2 cuv = (vec2(1.0 - _uv.x, _uv.y) - 0.5) * 2.0; // Centered UVs
        if (length(cuv) > 1.0)
            return vec4(0.0, 0.0, 0.0, 1.0);
    
        float phi = length(cuv) * (_fov / 360.0) * PI;
        vec2 dir = normalize(cuv);
        float alpha = atan(dir.y, dir.x);

        if (dir.x < 0.f && dir.y < 0.f)
        {
            alpha = atan(-dir.y, -dir.x);
            alpha += 3.0 * HALFPI;
        }
        else
        {
            alpha += HALFPI;
        }

        // This bloc moves the "view point" from the center of the dome
        vec3 directionVector = vec3(1.0, 0.0, 0.0);
        mat3 rotAlpha = mat3(cos(alpha), -sin(alpha), 0.0,
                             sin(alpha), cos(alpha), 0.0,
                             0.0, 0.0, 1.0);
        phi = HALFPI - phi;
        mat3 rotPhi = mat3(cos(phi), 0.0, sin(phi),
                           0.0, 1.0, 0.0,
                           -sin(phi), 0.0, cos(phi));
        directionVector = rotAlpha * rotPhi * directionVector;
        directionVector = normalize(directionVector - _position);
        alpha = atan(directionVector.y, directionVector.x);
        phi = HALFPI - atan(directionVector.z, length(directionVector.xy));

        if (directionVector.x < 0.f && directionVector.y < 0.f)
        {
            alpha = atan(-directionVector.y, -directionVector.x);
            alpha += 3.0 * HALFPI;
        }
        else
        {
            alpha += HALFPI;
        }

        // Compute the phi value to be in an horizontal camera, or outside
        float localAlpha = mod(alpha, HALFPI) - HALFPI / 2.0; // Alpha in the direction of an (undefined yet) horizontal camera
        float hPhi = HALFPI - phi; // Phi from the horizontal plane
        float d = 1.0 / cos(localAlpha); // Distance to camera center
        float h = d * tan(hPhi); // Vertical position along horizontal screens

        vec4 color;
        // -sqrt(2) < h < sqrt(2) --> the target is a horizontal camera
        if (abs(h) < 1.0)
        {
            vec2 localUV = vec2(tan(localAlpha) * 0.5 + 0.5, h * 0.5 + 0.5);

            if (alpha < HALFPI)
                color = texture(_texture0, localUV);
            else if (alpha < PI)
                color = texture(_texture1, localUV);
            else if (alpha < 3 * HALFPI)
                color = texture(_texture2, localUV);
            else
                color = texture(_texture3, localUV);
        }
        // else, the target is top or down camera
        else
        {
            // Top camera
            if (h >= 1.0)
            {
                float distToCenter = tan(phi);
                alpha += PI + HALFPI / 2.0;
                vec2 localUV = vec2((distToCenter * cos(alpha)) * 0.5 + 0.5, (distToCenter * sin(alpha)) * 0.5 + 0.5);
                color = texture(_texture5, localUV);
            }
            // Bottom camera
            else
            {
                float distToCenter = tan(PI - phi);
                alpha = -alpha + PI - HALFPI / 2.0;
                vec2 localUV = vec2((distToCenter * cos(alpha)) * 0.5 + 0.5, (distToCenter * sin(alpha)) * 0.5 + 0.5);
                color = texture(_texture4, localUV);
            }

        }

        return color;
    }

    vec4 renderPanorama()
    {
        vec2 cuv = vec2(_uv.s, _uv.t - 0.5) * 2.0; // Centered UVs
        float alpha = cuv.x * PI;
        float phi = cuv.y * HALFPI;

        float localAlpha = mod(alpha, HALFPI) - HALFPI / 2.0;
        float d = 1.0 / cos(localAlpha);
        float h = d * tan(phi);

        vec4 color;
        if (abs(h) < 1.0)
        {
            vec2 localUV = vec2(tan(localAlpha) * 0.5 + 0.5, h * 0.5 + 0.5);

            if (alpha < HALFPI)
                color = texture(_texture0, localUV);
            else if (alpha < PI)
                color = texture(_texture1, localUV);
            else if (alpha < HALFPI + PI)
                color = texture(_texture2, localUV);
            else
                color = texture(_texture3, localUV);
        }
        else
        {
            if (h >= 1.0)
            {
                float distToCenter = tan(HALFPI - phi);
                alpha = alpha - HALFPI * 1.5;
                vec2 localUV = vec2((distToCenter * cos(alpha)) * 0.5 + 0.5, (distToCenter * sin(alpha)) * 0.5 + 0.5);
                color = texture(_texture5, localUV);
            }
            else
            {
                float distToCenter = tan(HALFPI - phi);
                alpha = -alpha - HALFPI * 0.5;
                vec2 localUV = vec2((distToCenter * cos(alpha)) * 0.5 + 0.5, (distToCenter * sin(alpha)) * 0.5 + 0.5);
                color = texture(_texture4, localUV);
            }
        }

        return color;
    }

    vec4 renderCubemap()
    {
        if (_uv.t < 0.5)
        {
            if (_uv.s < 1.0 / 3.0)
            {
                vec2 localUV = vec2(_uv.s * 3.0, _uv.t * 2.0);
                return texture(_texture4, localUV);
            }
            else if (_uv.s < 2.0 / 3.0)
            {
                vec2 localUV = vec2(_uv.s * 3.0 - 1.0, _uv.t * 2.0);
                return texture(_texture5, localUV);
            }
            else
            {
                vec2 localUV = vec2(_uv.s * 3.0 - 2.0, _uv.t * 2.0);
                return texture(_texture0, localUV);
            }
        }
        else
        {
            if (_uv.s < 1.0 / 3.0)
            {
                vec2 localUV = vec2(_uv.s * 3.0, _uv.t * 2.0 - 1.0);
                return texture(_texture1, localUV);
            }
            else if (_uv.s < 2.0 / 3.0)
            {
                vec2 localUV = vec2(_uv.s * 3.0 - 1.0, _uv.t * 2.0 - 1.0);
                return texture(_texture2, localUV);
            }
            else
            {
                vec2 localUV = vec2(_uv.s * 3.0 - 2.0, _uv.t * 2.0 - 1.0);
                return texture(_texture3, localUV);
            }
        }
    }
    
    void main()
    {
        if (_renderMode == 0)
            _color = renderDome();
        else if (_renderMode == 1)
            _color = renderPanorama();
        else if (_renderMode == 2)
            _color = renderCubemap();
        else
            _color = vec4(0.0, 1.0, 0.0, 1.0);
    }
'''
